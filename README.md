Save a page or link to [archive.today](https://archive.today), or query it for older snapshots of the current page.

- [Download for Pale Moon](https://addons.palemoon.org/addon/recordrewind/).