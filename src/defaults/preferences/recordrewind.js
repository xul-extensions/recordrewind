pref("extensions.recordrewind.firstrun", true);
pref("extensions.recordrewind.logging", false);
pref("extensions.recordrewind.newtab",true);
pref("extensions.recordrewind.record","{\"name\":\"archive.today\",\"URL\":\"https://archive.today/?run=1&url=\",\"encode\":true}");
pref("extensions.recordrewind.rewind","{\"name\":\"archive.today\",\"URL\":\"https://archive.today/\",\"encode\":false}");