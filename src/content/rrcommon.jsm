var EXPORTED_SYMBOLS=['Common','RecRewindPrefListener'];
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
Common = {
	logoutput : false,
	uninstall : false,
	RRID : "{0FE2AE05-62E3-4C75-8B8B-90370C679274}",
	rrPrefBranch : Services.prefs.getBranch("extensions.recordrewind."),
	rrStrBundle : Services.strings.createBundle("chrome://recordrewind/locale/recordrewind.properties"),
	/**
	 * Toggle console logging.
	 */
	setLogging:function(flag){
		Common.logoutput = flag;
	},
	/**
	 * Log output to toolkit error console if enabled.
	 */ 
	printlog:function(item) {
		if(Common.logoutput==true) {
			Services.console.logStringMessage("[RecordRewind:]"+item);
		}
	},
	/**
	 * Install toolbar button.
	 */
	installButton:function(doc,toolbarId,id,afterId ){
		if (!doc.getElementById(id)) {
			var toolbar = doc.getElementById(toolbarId);
			var before = null;
			if (afterId) {
				let elem = doc.getElementById(afterId);
				if (elem && elem.parentNode == toolbar){
					before = elem.nextElementSibling;
				}
			}
			toolbar.insertItem(id,before);
			toolbar.setAttribute("currentset", toolbar.currentSet);
			doc.persist(toolbar.id, "currentset");
			if (toolbarId == "addon-bar"){
			toolbar.collapsed = false;
				}
		}
	},
	/**
	 * Check whether extension is being uninstalled and undo if not. 
	 */
	uninstallListener :{
		onUninstalling:function(addon){
			if(addon.id==Common.RRID){
				Common.uninstall=true;
			}
		},
		onOperationCancelled: function(addon) {
    		if (addon.id == Common.RRID) {
      			Common.uninstall = (addon.pendingOperations & AddonManager.PENDING_UNINSTALL) != 0;
      		}
    	}	
	},
	/**
	 * Clean up settings when browser is restarting after a confirmed uninstall.
	 */
	uninstallObserver :{
		observe : function(aSubject,aTopic,aData){
			if(aTopic == "quit-application-granted" && Common.uninstall == true) {
				Common.rrPrefBranch.clearUserPref("firstrun");
				Common.rrPrefBranch.clearUserPref("logging");
				Common.rrPrefBranch.clearUserPref("newtab");
				Common.rrPrefBranch.clearUserPref("record");
				Common.rrPrefBranch.clearUserPref("rewind");
				Common.rrPrefBranch.deleteBranch("");
			}
		}
	}
}
function RecRewindPrefListener(branch_name, callback) {
	// Keeping a reference to the observed preference branch or it will get
	// garbage collected.
	this._branch = Services.prefs.getBranch(branch_name);
	this._branch.QueryInterface(Ci.nsIPrefBranch2);
	this._callback = callback;
}

RecRewindPrefListener.prototype.observe = function(subject, topic, data) {
	if (topic == 'nsPref:changed')
		this._callback(this._branch, data);
};

RecRewindPrefListener.prototype.register = function() {
	this._branch.addObserver('', this, false);
	let that = this;
	this._branch.getChildList('', {}).
	forEach(function (pref_leaf_name){ 
		that._callback(that._branch, pref_leaf_name); 
	});
};

RecRewindPrefListener.prototype.unregister = function() {
	if (this._branch)
		this._branch.removeObserver('', this);
};	