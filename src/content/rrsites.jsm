var EXPORTED_SYMBOLS=['rrsites'];
rrsites = {
  "searchsites" :[
  {
    "name":"archive.today",
    "URL":"https://archive.today/",
    "encode":false
  },
  {
    "name":"archive.org",
    "URL":"https://web.archive.org/web/*/",
    "encode":false
  },
  {
    "name":"archive-it",
    "URL":"https://archive-it.org/explore?q=",
    "encode":false
  },
  {
    "name":"Trove (Australia)",
    "URL":"https://trove.nla.gov.au/search/category/websites?keyword=",
    "encode":false
  }
  ],
  "snapshots":[
  {
    "name":"archive.today",
    "URL":"https://archive.today/?run=1&url=",
    "encode":true
  },
  {
    "name" : "archive.org",
    "URL":"https://web.archive.org/save/",
    "encode":false
  }
  ]
};