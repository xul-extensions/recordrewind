if ("undefined" == typeof (RecRewind)) {
  var RecRewind = {};
}
Components.utils.import("chrome://recordrewind/content/rrcommon.jsm", RecRewind);
const { classes: Cc, interfaces: Ci, utils: Cu } = Components;
Cu.import("chrome://recordrewind/content/rrsites.jsm", RecRewind);
RecRewind.Options = {
  lstSearch: null,
  lstSnapshot: null,
  init: function() {
    RecRewind.Options.lstSearch = document.getElementById('rrpref-lst-search');
    RecRewind.Options.lstSnapshot = document.getElementById('rrpref-lst-snapshot');
    var defRecord = JSON.parse(RecRewind.Common.rrPrefBranch.getCharPref("record"));
    var defRewind = JSON.parse(RecRewind.Common.rrPrefBranch.getCharPref("rewind"));
    RecRewind.Options.loadList(RecRewind.Options.lstSearch, RecRewind.rrsites.searchsites, defRewind);
    RecRewind.Options.loadList(RecRewind.Options.lstSnapshot, RecRewind.rrsites.snapshots, defRecord);
    RecRewind.Options.lstSearch.addEventListener('select', RecRewind.Options.onSelect);
    RecRewind.Options.lstSnapshot.addEventListener('select', RecRewind.Options.onSelect);
  },
	/**
	 * Loads the specified list.
	 */
  loadList: function(list, array, defaultSite) {
    for (let i = 0; i < array.length; i++) {
      list.appendItem(array[i].name, JSON.stringify(array[i]));
      if (array[i].name == defaultSite.name) {
        list.selectedIndex = i;
      }
    }
  },
	/**
	 * Event listener for lists.   
	 */
  onSelect: function(event) {
    var property = event.target == RecRewind.Options.lstSearch ? "rewind" : "record";
    RecRewind.Common.rrPrefBranch.setCharPref(property, event.target.selectedItem.value);
    RecRewind.Common.printlog("Set " + property + " to " + event.target.selectedItem.value);
  },
  cleanup: function() {
    RecRewind.Options.lstSnapshot.removeEventListener('select', RecRewind.Options.onSelect);
    RecRewind.Options.lstSearch.removeEventListener('select', RecRewind.Options.onSelect);
  }
}
window.addEventListener("load", RecRewind.Options.init, false);
window.addEventListener("unload", RecRewind.Options.cleanup, false);