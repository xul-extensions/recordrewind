if ("undefined" == typeof (RecRewind)) {
  var RecRewind = {};
}
Components.utils.import("chrome://recordrewind/content/rrcommon.jsm", RecRewind);
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("resource://gre/modules/AddonManager.jsm");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
Cu.import("chrome://recordrewind/content/rrsites.jsm", RecRewind);
RecRewind.Launch = {
  cmdRecRew: null,
  cmdFind: null,
  cmdPrefs: null,
  cmdRecord: null,
  contextMenu: null,
  currURI: null,
  prefWindow: null,
  rrRecord: JSON.parse(RecRewind.Common.rrPrefBranch.getCharPref("record")),
  rrRewind: JSON.parse(RecRewind.Common.rrPrefBranch.getCharPref("rewind")),
  init: function() {
    let firstRun = RecRewind.Common.rrPrefBranch.getBoolPref("firstrun", true);
    if (firstRun) {
      RecRewind.Common.rrPrefBranch.setBoolPref("firstrun", false);
      RecRewind.Common.installButton(document, "nav-bar", "tbb-recrewind");
      RecRewind.Common.installButton(document, "addon-bar", "tbb-recrewind");
    }
    RecRewind.Common.setLogging(RecRewind.Common.rrPrefBranch.getBoolPref("logging", false));
    RecRewind.Launch.cmdPrefs = document.getElementById('CmdRRPrefs');
    RecRewind.Launch.cmdRecRew = document.getElementById('CmdRecRewToolbarButton');
    RecRewind.Launch.cmdRecord = document.getElementById('CmdRecord');
    RecRewind.Launch.cmdFind = document.getElementById('CmdFind');
    RecRewind.Launch.contextMenu = document.getElementById('contentAreaContextMenu');
    RecRewind.Launch.cmdPrefs.addEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.cmdRecRew.addEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.cmdFind.addEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.cmdRecord.addEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.contextMenu.addEventListener('popupshowing', RecRewind.Launch.onContextMenu);
    RecRewind.Launch.prefListener.register();
    gBrowser.addProgressListener(RecRewind.Launch.tabSwitchListener);
    AddonManager.addAddonListener(RecRewind.Common.uninstallListener);
    Services.obs.addObserver(RecRewind.Common.uninstallObserver, "quit-application-granted", false);
  },
	/**
	 * Context menu handler
	 */
  onContextMenu: function() {
    //if simply rightclicked, then use current page's URI, else use link.
    RecRewind.Launch.currURI = gBrowser.selectedBrowser.currentURI;
    if (gContextMenu.onLink) {
      RecRewind.Launch.currURI = gContextMenu.linkURI;
    }
    RecRewind.Common.printlog("In context menu, URL is " + RecRewind.Launch.currURI.spec);
    gContextMenu.showItem('menu-recrewind', RecRewind.Launch.isValidLink(RecRewind.Launch.currURI))
  },
  /**
   * Displays the preferences window.
   */
  showPrefs: function() {
    if (RecRewind.Launch.prefWindow == null || RecRewind.Launch.prefWindow.closed) {
      let instantApply =
        Services.prefs.getBoolPref("browser.preferences.instantApply", true);
      let features =
        "chrome,titlebar,centerscreen" +
        (instantApply.value ? ",dialog=no" : ",modal");
      RecRewind.Launch.prefWindow = window.openDialog("chrome://recordrewind/content/rrprefs.xul", "RecordRewind Preferences",
        features);
      RecRewind.Launch.prefWindow.focus();
    }
  },
	/**
	 * Checks whether the link is a regular web link.
     */
  isValidLink: function(aURI) {
    return (aURI.schemeIs('http') || aURI.schemeIs('https')) && aURI.host != 'localhost' && aURI.host!="127.0.0.1";
  },
	/**
	 * Toolbar button handler.
	 */
  onToolbarButton: function(event) {
    RecRewind.Common.printlog("On toolbar press, URL is " + RecRewind.Launch.currURI.spec);
    RecRewind.Common.printlog("Record URL is " + RecRewind.Launch.rrRecord.URL + "\nRewind URL is " + RecRewind.Launch.rrRewind.URL);
    let url = null;
    let target = null;
    if (event.target == RecRewind.Launch.cmdFind || event.target == RecRewind.Launch.cmdRecRew) {
      if (RecRewind.Launch.rrRewind.encode) {
        target = encodeURIComponent(RecRewind.Launch.currURI.spec);
      } else {
        target = RecRewind.Launch.currURI.spec;
      }
      url = RecRewind.Launch.rrRewind.URL + target;
    } else if (event.target == RecRewind.Launch.cmdRecord) {
      if (RecRewind.Launch.rrRecord.encode) {
        target = encodeURIComponent(RecRewind.Launch.currURI.spec);
      } else {
        target = RecRewind.Launch.currURI.spec;
      }
      url = RecRewind.Launch.rrRecord.URL + target;
    } else if (event.target == RecRewind.Launch.cmdPrefs) {
      RecRewind.Launch.showPrefs();
      return;
    }
    RecRewind.Launch.openTab(url);
    RecRewind.Launch.currURI = null;
  },
	/**
	 * Opens a new tab according to preference.
	 */
  openTab: function(url) {
    switch (RecRewind.Common.rrPrefBranch.getBoolPref("newtab", true)) {
      case (false):
        RecRewind.Common.printlog("Opening " + url + " in current tab");
        gBrowser.selectedBrowser.loadURI(url);
        break;
      case (true):
        RecRewind.Common.printlog("Opening " + url + " in new tab");
        gBrowser.addTab(url);
        break;
    }
  },
	/**
	 * Disable button on non webpages (such as about: pages).
	 */
  tabSwitchListener: {
    QueryInterface: XPCOMUtils.generateQI(["nsIWebProgressListener", "nsISupportsWeakReference"]),
    onStateChange: function(aWebProgress, aRequest, aFlag, aStatus) {
    },
    onLocationChange: function(aProgress, aRequest, aURI) {
      let tooltip = null;
	  RecRewind.Launch.currURI = aURI;
      if (RecRewind.Launch.isValidLink(aURI)) {
        RecRewind.Launch.cmdRecRew.removeAttribute('disabled');
        tooltip = "enabled";
      } else {
        RecRewind.Launch.cmdRecRew.setAttribute('disabled', 'true');
        tooltip = "disabled";
      }
      RecRewind.Launch.cmdRecRew.setAttribute("tooltiptext",
        RecRewind.Common.rrStrBundle.GetStringFromName(
          "toolbarbutton.tooltip." + tooltip));
    }
  },
  prefListener: new RecRewind.RecRewindPrefListener("extensions.recordrewind.", function(branch, name) {
    switch (name) {
      case "record":
        RecRewind.Launch.rrRecord = JSON.parse(branch.getCharPref("record"));
        RecRewind.Common.printlog("Using " + RecRewind.Launch.rrRecord.name + " for recording.");
        break;
      case "rewind":
        RecRewind.Launch.rrRewind = JSON.parse(branch.getCharPref("rewind"));
        RecRewind.Common.printlog("Using " + RecRewind.Launch.rrRewind.name + " for rewinding.");
        break;
    }
  }),
	/**
	 * Cleanup listeners on exit.
	 */
  cleanup: function() {
    Services.obs.removeObserver(RecRewind.Common.uninstallObserver, "quit-application-granted");
    AddonManager.removeAddonListener(RecRewind.Common.uninstallListener);
    gBrowser.removeProgressListener(RecRewind.Launch.tabSwitchListener);
    RecRewind.Launch.prefListener.unregister();
    RecRewind.Launch.contextMenu.removeEventListener('popupshowing', RecRewind.Launch.onContextMenu);
    RecRewind.Launch.cmdRecord.removeEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.cmdFind.removeEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.cmdRecRew.removeEventListener('command', RecRewind.Launch.onToolbarButton);
    RecRewind.Launch.cmdPrefs.removeEventListener('command', RecRewind.Launch.onToolbarButton);
  }
}
window.addEventListener("load", RecRewind.Launch.init, false);
window.addEventListener("unload", RecRewind.Launch.cleanup, false);